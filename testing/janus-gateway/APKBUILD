# Contributor: Michael Pirogov <vbnet.ru@gmail.com>
# Maintainer: Michael Pirogov <vbnet.ru@gmail.com>
pkgname=janus-gateway
pkgver=1.0.2
pkgrel=0
pkgdesc="Janus WebRTC Server"
url="https://janus.conf.meetecho.com/"
license="GPL-3.0-only"
arch="all !riscv64" # blocked by nodejs/npm
install="$pkgname.pre-install"
makedepends="autoconf automake libtool gengetopt lua5.3-dev cmake libsrtp-dev
	libnice-dev jansson-dev libconfig-dev libusrsctp-dev libmicrohttpd-dev
	libwebsockets-dev rabbitmq-c-dev curl-dev libogg-dev libopusenc-dev
	lua duktape-dev npm doxygen graphviz ffmpeg-dev zlib-dev libogg-dev
	libuv-dev sofia-sip-dev paho-mqtt-c-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/meetecho/janus-gateway/archive/refs/tags/v$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd
	websocket-alpine.patch
	"
subpackages="$pkgname-doc $pkgname-openrc"
options="!check" # missing aiortc

# nanomsg not available on armv7
case "$CARCH" in
	arm*)
		;;
	*)
		makedepends="$makedepends nanomsg-dev"
		;;
esac

prepare() {
	default_prepare
	autoreconf -fi
}

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--runstatedir=/run \
		--enable-sample-event-handler \
		--enable-rest \
		--enable-javascript-es-module \
		--enable-all-js-modules \
		--enable-post-processing \
		--enable-json-logger \
		--enable-plugin-lua \
		--enable-plugin-sip \
		--enable-plugin-duktape \
		--enable-plugin-mqtt \
		--enable-docs \
		--disable-aes-gcm
	make
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
20ff20ccab40f45879e127a2c1f3e5ebde1567b393d59f862b38805045e2a0cc379f7dfd5566a89d1ef644552a2936d0a11720e4e1d7bcc45b136c57d26ef139  janus-gateway-1.0.2.tar.gz
942d8566219a426671d78b962f1584a910137995a2dbfc9af2a5c2901f30dc8992bbc749543d149d9f3d24bd751090abc226fca405276450ef84acff1cd74cc1  janus-gateway.initd
f442a419a435f5d1adab673011b7689a5680064f32f712e5e4668c486ce10f1442822c60cb302ee850ddc576a9e9f610a0863f02204e56f1fc68aa3ee312ebe0  janus-gateway.confd
0d8b67b91a69f4f6d98f52bf703a517be3a47c2fcbcaa57f7bc858d1dee441cc423ab8792f7467ef8c7e2714ccc2bd65a919a5d7979f49494fed4ec120a8ed6e  websocket-alpine.patch
"
