# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ruqola
pkgver=1.7.1
pkgrel=0
pkgdesc="A Qt/QML client for Rocket Chat"
url="https://invent.kde.org/network/ruqola"
# armhf blocked by qt5-qtdeclarative-dev
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
license="GPL-2.0-only OR GPL-3.0-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	knotifications-dev
	knotifyconfig-dev
	ktextwidgets-dev
	kuserfeedback-dev
	kxmlgui-dev
	libsecret-dev
	prison-dev
	qt5-qtbase-dev
	qt5-qtkeychain-dev
	qt5-qtmultimedia-dev
	qt5-qtnetworkauth-dev
	qt5-qtwebsockets-dev
	samurai
	sonnet-dev
	syntax-highlighting-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/ruqola/ruqola-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# Skip broken tests
	local skipped_tests="("
	local tests="
		roomscleanhistoryjob
		roomsexportjob
		user
		channelcounterinfo
		roomheaderwidget
		messagelinewidget
		messagelistdelegate
		inviteuserswidget
		showvideowidget
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0a9351bec775f6bec363deed6f211c195cb5791898a9567f2841c48b0e178fae6f7a0dc9c1fa13621bb12d9b143b1b2fba57b47da5fb3217a4e8423c68003fc2  ruqola-1.7.1.tar.xz
"
