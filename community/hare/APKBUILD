# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=hare
pkgver="0_git20220528"
_commit=19e380ccb7dfe2bcab5f94e6bd03004e3e2c6005
pkgrel=0
pkgdesc="The Hare systems programming language"
url="https://harelang.org"
# riscv64: FTBFS: Abort: ./os/exec/cmd.ha:79:1: execution reached unreachable code (compiler bug)
# Caused by qemu-user, see https://gitlab.com/qemu-project/qemu/-/issues/1007
arch="x86_64 aarch64"
license="MPL-2.0 AND GPL-3.0-only"
depends="qbe harec binutils"
makedepends="scdoc"
subpackages="$pkgname-doc"
source="
	$pkgname-$_commit.tar.gz::https://git.sr.ht/~sircmpwn/hare/archive/$_commit.tar.gz
	config.aarch64.mk
	config.riscv64.mk
	config.x86_64.mk
"
builddir="$srcdir/$pkgname-$_commit"

build() {
	cp "$srcdir"/config.$CARCH.mk ./config.mk
	export VERSION="dev+$(echo "$_commit" | cut -c-7)"
	export LOCALVER=alpine
	make -j1 # XXX: parallel build driver builds are broken
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
507320952af3cad184366fa12f3bb255a8e29e30611a309161120b2cc04a89639760882775f3a9bc44d1cacdcee45773a5b78bfb01a85c2e9c6c3aa706e4e791  hare-19e380ccb7dfe2bcab5f94e6bd03004e3e2c6005.tar.gz
c30c2979a94d73f5bbb39c3fa250d155548fb07ba93cb4a2111eede51be5fd9017e2b4291f4f16657718e6bc414b90b583a14df572fab4579d26c5680a279dcb  config.aarch64.mk
6980757c09987363817de0599b59334817831f1c5782ae48a94b88c5b10aa1ab32fdc6c76911d02d1c100d78db98f1ca2167ecdf9580fc4d9bc560577f6fe89e  config.riscv64.mk
159c2f56f9617a97ecbe2b1ab94c98beb798fa575463bdcfba281a023202df481714c16536e213e859468b50cdd9f339224d8a3b3d57e139a58412b3efba6d84  config.x86_64.mk
"
