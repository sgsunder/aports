# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktnef
pkgver=22.04.1
pkgrel=0
pkgdesc="API for handling TNEF data"
# armhf blocked by extra-cmake-modules
# s390x blocked by kcalendarcore
# riscv64 blocked by polkit -> kcalutils
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcalutils-dev
	kcontacts-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktnef-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
72ae1e3a3e46244b2fa08ca5e77c0326d2fff73bc9c079b8edf60e42d27be40dec7b88a18ccdeb9cf9533463707d3e41b681db8d0134523bc86a046b0d504bb1  ktnef-22.04.1.tar.xz
"
