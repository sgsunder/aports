# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bovo
pkgver=22.04.1
pkgrel=0
pkgdesc="A Gomoku like game for two players"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/games/org.kde.bovo"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/bovo-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d277ec16e967e7b7faf77c8de2b2871ed5339dcda64da0b9ad60f381ef29c3f9668f73bfd9e662884582f6395ca60b39a99f6fee54e916f4b15d788010d9c48f  bovo-22.04.1.tar.xz
"
