# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdegraphics-thumbnailers
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio -> polkit
arch="all !armhf !s390x !riscv64"
pkgdesc="Thumbnailers for various graphics file formats"
url="https://www.kde.org/applications/graphics/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdegraphics-mobipocket-dev
	kio-dev
	libkdcraw-dev
	libkexiv2-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdegraphics-thumbnailers-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
069a52dfb447b7a4419132f25e688180c0bfa22f2d2348ce3393a901869b047241dbd8a5c03b23ebb0e894a00b91ff7266e4d1a2a6906acfcaa92239879465c5  kdegraphics-thumbnailers-22.04.1.tar.xz
"
