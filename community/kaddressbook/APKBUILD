# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaddressbook
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/components/kaddressbook.html"
pkgdesc="Address Book application to manage your contacts"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends="kdepim-runtime"
makedepends="
	akonadi-dev
	akonadi-search-dev
	extra-cmake-modules
	gpgme-dev
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kontactinterface-dev
	kpimtextedit-dev
	kuserfeedback-dev
	kuserfeedback-dev
	libkdepim-dev
	libkleo-dev
	pimcommon-dev
	prison-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaddressbook-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
0a79c60744c9100f3e68438ca0b831453adfe761af8c49b2e065f10259af3939f17ffd00d2327b9fe693088b0c0a7ccb5ca227c7982d9c07a223e9ec2c2f3501  kaddressbook-22.04.1.tar.xz
"
