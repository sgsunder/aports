# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-dev-utils
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kparts-dev
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/development/"
pkgdesc="Small utilities for developers using KDE/Qt libs/frameworks"
license="(LGPL-2.0-only OR LGPL-3.0-only) AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-dev-utils-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7e9ba24624f6c0d9b51c2f146b70336d07f968ce0dd8a214ece0b0f304762610d7663e74291a268d68c681a0291527e936f863ede3912200bb3516db720f5210  kde-dev-utils-22.04.1.tar.xz
"
