# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=glances
pkgver=3.2.6.1
pkgrel=0
pkgdesc="CLI curses based monitoring tool"
url="https://nicolargo.github.io/glances/"
arch="noarch"
license="LGPL-3.0-or-later"
depends="py3-future py3-psutil py3-bottle py3-snmp py3-batinfo docker-py py3-defusedxml"
makedepends="py3-setuptools"
subpackages="$pkgname-doc"
source="glances-$pkgver.tar.gz::https://github.com/nicolargo/glances/archive/v$pkgver.tar.gz"
options="!check"  # tests fail on builder infra due to env. limitations

build() {
	python3 setup.py build
}

check() {
	python3 unitest.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Remove images and HTML doc depending on those
	rm -rf "$pkgdir"/usr/share/doc/$pkgname/$pkgname-doc.html \
		"$pkgdir"/usr/share/doc/$pkgname/images
}

sha512sums="
be7ec70b528db5cfe6f98e35a5e435dc84ddb4fee2a432131a68cef23f5e010757f47d6c4998d8c3fb95a508ed0525eb125d6797e493a4a23c0ea396368d1c88  glances-3.2.6.1.tar.gz
"
