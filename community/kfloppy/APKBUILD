# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kfloppy
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kfloppy"
pkgdesc="A utility that provides a straightforward graphical means to format 3.5\" and 5.25\" floppy disks"
license="GPL-2.0-or-later"
depends="
	coreutils
	dosfstools
	e2fsprogs
	"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kfloppy-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e0f57c0fd0271f7401e1a29f7986b7a8a6fdaf29090ca6ebd1e3e7911128b9ffa8ee76e418dcae146005ed11aaae1ebfaac2bc1bbaf368aa79f925d212a82003  kfloppy-22.04.1.tar.xz
"
