# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kcron
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/system/"
pkgdesc="Configure and schedule tasks"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcron-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
573a97bc833e471dd32f9ea2a5afee6267d723a4d8991150814792eb1141e02859a1bb44080111ad409393850b02e25220ddbc4d6781fcdc6279febddf01beb2  kcron-22.04.1.tar.xz
"
